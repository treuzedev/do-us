import 'package:dome/classes/strings.dart';
import 'package:dome/database/database_helpers.dart';
import 'package:dome/database/database_model_task.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

//
// share data between screens
class SharedViewModel extends ChangeNotifier {
  //
  // properties
  String _taskString = Strings.nullLabel;
  sqflite.Database _database;
  List<Task> _taskList;
  String _counter;
  bool _triggerListViewRebuild = false;

  //
  // task string
  void setTaskString(String string) {
    _taskString = string;
    notifyListeners();
  }

  String getTaskString() {
    return _taskString;
  }

  //
  // get database instance
  Future<sqflite.Database> getDatabaseInstance() async {
    //
    // ensure there is something to return
    if (_database == null) {
      _database = await DatabaseHelpers.openDatabase();
    }

    //
    // return database
    return _database;
  }

  //
  // update task list
  void setTaskList(List<Task> list) {
    _taskList = list;
  }

  List<Task> getTaskList() {
    //
    // ensure something is returned
    if (_taskList == null) {
      _taskList = [];
    }

    //
    // return
    return _taskList;
  }

  //
  // get counter
  String getCounterValue(SharedViewModel sharedViewModel) {
    //
    // ensure something is returned
    if (_counter == null) {
      DatabaseHelpers.getCounter(sharedViewModel: sharedViewModel);
      return '...';
    }

    //
    // return counter
    return _counter;
  }

  //
  // set counter
  // notify listeners to update ui
  void setCounterValue(String counter) {
    _counter = counter;
    notifyListeners();
  }

  //
  // list view rebuild
  // notify listeners to rebuild ui
  void setListViewRebuildFlag() {
    _triggerListViewRebuild = !_triggerListViewRebuild;
    notifyListeners();
  }

  bool getListViewRebuildFlag() {
    return _triggerListViewRebuild;
  }
}
