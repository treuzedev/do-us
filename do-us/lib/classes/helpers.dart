import 'package:dome/classes/colors.dart';
import 'package:dome/classes/strings.dart';
import 'package:dome/widgets/round_material_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:provider/provider.dart';
import '../database/database_helpers.dart';
import 'my_navigator.dart';
import 'shared_view_model.dart';
import 'sizes.dart';

//
// a class to hold functions shared across the app
class Helpers {
  //
  // methods
  static double getBlockSize(BuildContext context) {
    return (MediaQuery.of(context).size.height / 100);
  }

  //
  // app bar
  static Widget myAppBar({
    @required bool flag,
    @required double blockSize,
    @required BuildContext context,
  }) {
    //
    // get a shared view model reference
    SharedViewModel sharedViewModel = Provider.of(context);

    //
    // return app bar
    // show a delete icon on home screen
    return AppBar(
      title: Text(Strings.appTitle),
      centerTitle: true,
      actions: flag == true
          ? appBarDeleteButton(
              context: context,
              blockSize: blockSize,
              sharedViewModel: sharedViewModel,
            )
          : null,
    );
  }

  //
  // app bar action button
  static List<Widget> appBarDeleteButton({
    @required double blockSize,
    @required SharedViewModel sharedViewModel,
    @required BuildContext context,
  }) {
    return [
      InkWell(
        child: Padding(
          padding: Sizes.getGeneralPadding(blockSize),
          child: Icon(Icons.delete_forever),
        ),
        onTap: () {
          //
          // show dialog
          Helpers.showMyDialog(
            context: context,
            blockSize: blockSize,
            title: Strings.deleteDialog,
            yesOnPressed: () async {
              //
              // delete all tasks
              await DatabaseHelpers.deleteAllTasks(sharedViewModel);

              //
              // go back
              Navigator.pop(context);
              Navigator.popAndPushNamed(context, MyNavigator.homeRoute);
            },
            noOnPressed: () {
              //
              // pop dialog
              Navigator.pop(context);
            },
          );
        },
      ),
    ];
  }

  //
  // dialogs
  static void showMyDialog({
    @required BuildContext context,
    @required String title,
    @required double blockSize,
    @required Function yesOnPressed,
    @required Function noOnPressed,
  }) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: MyColors.darkColor,
          title: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          content: Row(
            children: <Widget>[
              Expanded(
                child: RoundMaterialButton(
                  text: Strings.yesLabel,
                  color: MyColors.mediumLightColor,
                  blockSize: blockSize,
                  onPressed: yesOnPressed,
                ),
              ),
              Expanded(
                child: RoundMaterialButton(
                  text: Strings.noLabel,
                  color: MyColors.mediumDarkColor,
                  blockSize: blockSize,
                  onPressed: noOnPressed,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
