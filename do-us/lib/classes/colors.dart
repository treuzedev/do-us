import 'package:flutter/material.dart';

//
// a class to hold color constants
class MyColors {
  //
  // 0xff (opaque) to 0x00 (transparent)
  static const Color bottomSheetTransparentLikeColor = Color(0xff757575);
  static const Color lightColor = Color(0xfffce2db);
  static const Color mediumLightColor = Color(0xfff1c6d3);
  static const Color mediumDarkColor = Color(0xffe4a3d4);
  static const Color darkColor = Color(0xffc295d8);
}
