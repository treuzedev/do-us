//
// a class to handle navigation affairs
class MyNavigator {
  //
  // routes
  static final String homeRoute = 'home';
  static const String aboutRoute = 'about';
  static const String contactsRoute = 'contacts';
}
