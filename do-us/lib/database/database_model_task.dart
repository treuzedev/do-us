import 'package:flutter/cupertino.dart';

//
// a class to store database data
// is completed = 0, is not completed = 1
class Task {
  //
  // constructor
  Task({
    this.id,
    @required this.title,
    this.isCompleted = 1,
    @required this.timeStamp,
  });

  final int id;
  final String title;
  int isCompleted;
  final String timeStamp;
}
