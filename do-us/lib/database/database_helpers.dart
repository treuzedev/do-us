import 'package:dome/classes/shared_view_model.dart';
import 'package:flutter/material.dart';
import 'package:dome/database/database_model_task.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import '../classes/shared_view_model.dart';

//
// a class to do database work
class DatabaseHelpers {
  //
  // create table tasks onCreate
  static const tasksTableName = 'tasks';
  static const idColumn = 'id';
  static const titleColumn = 'title';
  static const isCompletedColumn = 'isCompleted';
  static const timeStampColumn = 'timeStamp';
  static const createTasksTable =
      'create table $tasksTableName($idColumn integer primary key, $titleColumn text, $isCompletedColumn integer, $timeStampColumn text)';

  //
  // create table to store statistics
  static const counterTableName = 'counterTable';
  static const countTasksColumn = 'counter';
  static const createCounterTable =
      'create table $counterTableName($idColumn integer primary key, $countTasksColumn int)';

  //
  // open database
  static Future<sqflite.Database> openDatabase() async {
    //
    // get system path for database
    // get database name
    // join
    String databasePath = await sqflite.getDatabasesPath();
    String databaseName = '-database.db';
    String finalPath = databasePath + databaseName;

    //
    // create connection
    // create tables
    // return connection
    sqflite.Database database = await sqflite.openDatabase(
      finalPath,
      version: 1,
      onCreate: (db, version) async {
        await db.execute(createTasksTable);
        await db.execute(createCounterTable);
      },
    );
    return database;
  }

  //
  // insert a task into a database
  static Future<void> insertTask({
    @required Task task,
    @required SharedViewModel sharedViewModel,
  }) async {
    //
    // get database connection
    sqflite.Database database = await sharedViewModel.getDatabaseInstance();

    //
    // insert data
    await database.rawInsert(
      'insert into $tasksTableName($idColumn, $titleColumn, $isCompletedColumn, $timeStampColumn) values (?, ?, ?, ?)',
      [
        task.id,
        task.title,
        task.isCompleted,
        task.timeStamp,
      ],
    );
  }

  //
  // retrive all table entries
  static Future<bool> getAllTasks({
    @required SharedViewModel sharedViewModel,
  }) async {
    //
    // get database connection
    sqflite.Database database = await sharedViewModel.getDatabaseInstance();

    //
    // get all entries
    final List<Map<String, dynamic>> maps =
        await database.rawQuery('select * from $tasksTableName');

    //
    // format maps into tasks
    final List<Task> list = [];
    maps.forEach((element) {
      list.add(Task(
        id: element[idColumn],
        title: element[titleColumn],
        isCompleted: element[isCompletedColumn],
        timeStamp: element[timeStampColumn],
      ));
    });

    //
    // update task list
    sharedViewModel.setTaskList(list);

    //
    // return
    return true;
  }

  //
  // delete a task
  static Future<void> deleteTask({
    @required int id,
    @required SharedViewModel sharedViewModel,
  }) async {
    //
    // get database connection
    sqflite.Database database = await sharedViewModel.getDatabaseInstance();

    //
    // delete task
    await database.rawDelete(
      'delete from $tasksTableName where $idColumn = ?',
      [id],
    );
  }

  //
  // update checkbox value
  static Future<void> updateTaskCheckbox({
    @required int id,
    @required int value,
    @required SharedViewModel sharedViewModel,
  }) async {
    //
    // get database connection
    sqflite.Database database = await sharedViewModel.getDatabaseInstance();

    //
    // update task
    await database.rawUpdate(
      'update $tasksTableName set $isCompletedColumn = ? where $idColumn = ?',
      [
        value,
        id,
      ],
    );
  }

  //
  // update counter
  static Future<void> updateCounter({
    @required SharedViewModel sharedViewModel,
    @required String oldValue,
  }) async {
    //
    // get database connection
    sqflite.Database database = await sharedViewModel.getDatabaseInstance();

    //
    // get new value
    // update ui
    String newValue = (int.parse(oldValue) + 1).toString();
    sharedViewModel.setCounterValue(newValue);

    //
    // update database
    await database.rawUpdate(
      'update $counterTableName set $countTasksColumn = ? where id = 1',
      [
        newValue,
      ],
    );
  }

  //
  // get counter
  static Future<void> getCounter({
    @required SharedViewModel sharedViewModel,
  }) async {
    //
    // get database connection
    sqflite.Database database = await sharedViewModel.getDatabaseInstance();

    //
    // get list of counters
    List<Map<String, dynamic>> tmp = await database
        .rawQuery('select * from $counterTableName where $idColumn = 1');

    //
    // check for null value the first time it is used
    // return the actual value otherwise
    if (tmp == null || tmp.isEmpty) {
      //
      // insert a 0 as the initial value
      await database.rawInsert(
        'insert into $counterTableName($idColumn, $countTasksColumn) values (?, ?)',
        [
          null,
          0.toString(),
        ],
      );

      //
      // return 0 as the initial values
      sharedViewModel.setCounterValue('0');
    }
    //
    else {
      sharedViewModel.setCounterValue(tmp[0][countTasksColumn].toString());
    }
  }

  //
  // delete all tasks
  static Future<void> deleteAllTasks(SharedViewModel sharedViewModel) async {
    //
    // get database connection
    sqflite.Database database = await sharedViewModel.getDatabaseInstance();

    //
    // get current number of tasks
    int numberOfTasks = sharedViewModel.getTaskList().length - 1;
    String newCounterValue = numberOfTasks.toString();

    //
    // update counter
    await updateCounter(
      sharedViewModel: sharedViewModel,
      oldValue: newCounterValue,
    );

    //
    // delete all data
    await database.rawDelete('delete from $tasksTableName');
  }

  //
  // update a task
  static Future<void> updateTaskTitle({
    @required int id,
    @required String newText,
    @required SharedViewModel sharedViewModel,
  }) async {
    //
    // get database connection
    sqflite.Database database = await sharedViewModel.getDatabaseInstance();

    await database.rawUpdate(
      'update $tasksTableName set $titleColumn = ? where id = ?',
      [
        newText,
        id,
      ],
    );
  }
}
