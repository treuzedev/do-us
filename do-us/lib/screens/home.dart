import 'package:dome/classes/helpers.dart';
import 'package:dome/classes/strings.dart';
import 'package:dome/widgets/counter.dart';
import 'package:dome/widgets/drawer_menu.dart';
import 'package:dome/widgets/round_material_button.dart';
import 'package:dome/widgets/tasks_list_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:dome/classes/colors.dart';
import 'package:provider/provider.dart';
import '../classes/shared_view_model.dart';
import '../database/database_helpers.dart';
import '../database/database_model_task.dart';
import 'home_bottom_sheet.dart';

//
// home screen
class Home extends StatelessWidget {
  //
  // build method
  @override
  Widget build(BuildContext context) {
    //
    // block size
    double blockSize = Helpers.getBlockSize(context);

    //
    // return widget
    return Scaffold(
      appBar: Helpers.myAppBar(
        context: context,
        flag: true,
        blockSize: blockSize,
      ),
      drawer: MyDrawerMenu(blockSize: blockSize),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: DuUsCounter(blockSize),
            ),
            Expanded(
              flex: 10,
              child: TasksListView(blockSize),
            ),
            Expanded(
              flex: 2,
              child: RoundMaterialButton(
                blockSize: blockSize,
                text: Strings.newTaskLabel,
                color: MyColors.darkColor,
                onPressed: () {
                  //
                  // show bottom sheet
                  showModalBottomSheet(
                    isScrollControlled: true,
                    context: context,
                    builder: (context) {
                      //
                      // variable to hold text input
                      String newTaskString;

                      //
                      // return bottom sheet
                      return Consumer<SharedViewModel>(
                        builder: (context, sharedViewModel, child) {
                          return HomeBottomSheet(
                            blockSize: blockSize,
                            onTextChanged: (newValue) {
                              newTaskString = newValue;
                            },
                            buttonOnPressed: () async {
                              //
                              // create a new task to add to database
                              Task task = Task(
                                title: newTaskString,
                                timeStamp: Strings.formatDate(
                                    DateTime.now().toString()),
                              );

                              //
                              // insert task in database
                              await DatabaseHelpers.insertTask(
                                  task: task, sharedViewModel: sharedViewModel);
                              Navigator.pop(context);
                            },
                          );
                        },
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
