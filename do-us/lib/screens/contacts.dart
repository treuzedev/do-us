import 'package:dome/classes/colors.dart';
import 'package:dome/classes/helpers.dart';
import 'package:dome/classes/my_navigator.dart';
import 'package:dome/classes/sizes.dart';
import 'package:dome/classes/strings.dart';
import 'package:dome/widgets/drawer_menu.dart';
import 'package:flutter/material.dart';

//
// contacts
class Contacts extends StatelessWidget {
  //
  // build method
  @override
  Widget build(BuildContext context) {
    //
    // block size
    double blockSize = Helpers.getBlockSize(context);

    //
    // return widget
    return WillPopScope(
      onWillPop: () {
        //
        // navigate to home screen
        Navigator.popAndPushNamed(
          context,
          MyNavigator.homeRoute,
        );

        //
        // return something
        return Future(() => true);
      },
      child: Scaffold(
        appBar: Helpers.myAppBar(
          context: context,
          flag: false,
          blockSize: blockSize,
        ),
        drawer: MyDrawerMenu(
          blockSize: blockSize,
        ),
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: Sizes.getGeneralPadding(blockSize),
                child: Text(
                  Strings.contactsText,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: MyColors.darkColor,
                    fontSize: Sizes.contactsFontSize(blockSize),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
