import 'package:dome/classes/colors.dart';
import 'package:dome/screens/about.dart';
import 'package:dome/screens/contacts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'classes/my_navigator.dart';
import 'classes/shared_view_model.dart';
import 'screens/home.dart';

//
// start main app
void main() => runApp(MyApp());

//
// my app
class MyApp extends StatelessWidget {
  //
  // sharedViewModel
  final SharedViewModel sharedViewModel = SharedViewModel();

  //
  // return widget
  @override
  Widget build(BuildContext context) {
    //
    // return app
    return AnnotatedRegion(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        systemNavigationBarColor: MyColors.lightColor,
      ),
      child: ChangeNotifierProvider(
        create: (context) => sharedViewModel,
        child: MaterialApp(
          theme: ThemeData.light().copyWith(
            accentColor: MyColors.mediumLightColor,
            scaffoldBackgroundColor: MyColors.lightColor,
            appBarTheme: AppBarTheme(color: MyColors.darkColor),
            textTheme: GoogleFonts.ubuntuTextTheme(Theme.of(context).textTheme),
          ),
          routes: {
            MyNavigator.homeRoute: (context) => Home(),
            MyNavigator.aboutRoute: (context) => About(),
            MyNavigator.contactsRoute: (context) => Contacts(),
          },
          initialRoute: MyNavigator.homeRoute,
        ),
      ),
    );
  }
}
