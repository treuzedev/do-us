import 'package:dome/classes/colors.dart';
import 'package:dome/classes/helpers.dart';
import 'package:dome/classes/shared_view_model.dart';
import 'package:dome/classes/sizes.dart';
import 'package:dome/classes/strings.dart';
import 'package:dome/database/database_helpers.dart';
import 'package:dome/database/database_model_task.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import '../database/database_helpers.dart';
import '../screens/home_bottom_sheet.dart';

//
// a class to display a task tile
class TaskTile extends StatefulWidget {
  //
  // constructor
  TaskTile(this.task);

  final Task task;

  @override
  _TaskTileState createState() => _TaskTileState();
}

class _TaskTileState extends State<TaskTile> {
  //
  // initial values
  bool isCompleted;

  @override
  void initState() {
    //
    // set iniital value
    isCompleted = widget.task.isCompleted == 0 ? true : false;

    super.initState();
  }

  //
  // return widget
  @override
  Widget build(BuildContext context) {
    //
    // get block size
    double blockSize = Helpers.getBlockSize(context);

    //
    // return widget
    return Consumer<SharedViewModel>(
      builder: (context, sharedViewModel, child) {
        return Padding(
          padding: Sizes.getGeneralPadding(blockSize),
          child: Material(
            borderRadius: Sizes.getGeneralBorderRadius(blockSize),
            color: MyColors.darkColor,
            child: InkWell(
              child: Padding(
                padding: Sizes.getGeneralPadding(blockSize),
                child: ListTile(
                  title: Text(
                    widget.task.title ?? Strings.nullLabel,
                    style: TextStyle(
                      fontSize: Sizes.taskTitleFontSize(blockSize),
                      color: Colors.white,
                      decoration: isCompleted == true
                          ? TextDecoration.lineThrough
                          : null,
                    ),
                  ),
                  subtitle: Padding(
                    padding: EdgeInsets.only(
                      top: 16,
                    ),
                    child: Text(
                      widget.task.timeStamp,
                      style: TextStyle(
                        fontSize: Sizes.timeFontSize(blockSize),
                        color: Colors.white,
                      ),
                    ),
                  ),
                  trailing: Theme(
                    data: ThemeData(
                      unselectedWidgetColor: Colors.white,
                    ),
                    child: Checkbox(
                      activeColor: MyColors.darkColor,
                      value: isCompleted,
                      onChanged: (flag) async {
                        //
                        // change check box appearance
                        setState(() {
                          isCompleted = flag;
                        });

                        //
                        // change value in database
                        int value = flag == true ? 0 : 1;
                        await DatabaseHelpers.updateTaskCheckbox(
                          id: widget.task.id,
                          value: value,
                          sharedViewModel: sharedViewModel,
                        );
                      },
                    ),
                  ),
                  onLongPress: () async {
                    //
                    // delete task from database
                    await DatabaseHelpers.deleteTask(
                      id: widget.task.id,
                      sharedViewModel: sharedViewModel,
                    );

                    //
                    // update counter
                    await DatabaseHelpers.updateCounter(
                      sharedViewModel: sharedViewModel,
                      oldValue:
                          sharedViewModel.getCounterValue(sharedViewModel),
                    );

                    //
                    // show snackbar
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: MyColors.mediumDarkColor,
                        content: Text(
                          Strings.deletedLabel,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        duration: Duration(milliseconds: 1000),
                      ),
                    );
                  },
                ),
              ),
              onDoubleTap: () {
                //
                // show bottom sheet
                showModalBottomSheet(
                  isScrollControlled: true,
                  context: context,
                  builder: (context) {
                    //
                    // variable to hold text input
                    String newTaskString;

                    //
                    // return bottom sheet
                    return Consumer<SharedViewModel>(
                      builder: (context, sharedViewModel, child) {
                        return HomeBottomSheet(
                          blockSize: blockSize,
                          onTextChanged: (newValue) {
                            newTaskString = newValue;
                          },
                          buttonOnPressed: () async {
                            //
                            // update task title
                            await DatabaseHelpers.updateTaskTitle(
                              id: widget.task.id,
                              newText: newTaskString,
                              sharedViewModel: sharedViewModel,
                            );

                            //
                            // pop view
                            Navigator.pop(context);
                          },
                        );
                      },
                    );
                  },
                );
              },
            ),
          ),
        );
      },
    );
  }
}
