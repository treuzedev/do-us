import 'package:dome/classes/colors.dart';
import 'package:dome/classes/shared_view_model.dart';
import 'package:dome/classes/sizes.dart';
import 'package:dome/classes/strings.dart';
import 'package:dome/database/database_helpers.dart';
import 'package:dome/database/database_model_task.dart';
import 'package:dome/widgets/task_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//
// a widget to display tasks
class TasksListView extends StatelessWidget {
  //
  // constructor
  TasksListView(this.blockSize);

  final double blockSize;
  //
  // build method
  @override
  Widget build(BuildContext context) {
    //
    // return widget
    return Padding(
      padding: Sizes.getGeneralPadding(blockSize),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: Sizes.getGeneralBorderRadius(blockSize),
        ),
        child: Consumer<SharedViewModel>(
          builder: (context, sharedViewModel, child) {
            return FutureBuilder<bool>(
              future:
                  DatabaseHelpers.getAllTasks(sharedViewModel: sharedViewModel),
              builder: (context, snapshot) {
                print(snapshot.data);
                if (snapshot.data == null) {
                  return Center(
                    child: Container(
                        height: Sizes.getCircularProgressSizes(blockSize),
                        width: Sizes.getCircularProgressSizes(blockSize),
                        child: CircularProgressIndicator()),
                  );
                }
                //
                else {
                  //
                  // rebuild widget if all entries are deleted
                  final bool flag = sharedViewModel.getListViewRebuildFlag();
                  print('REBUILD TRIGGER -> flag valued changed to -> $flag');

                  //
                  // get a list of tasks
                  final List<Task> list = sharedViewModel.getTaskList();

                  //
                  // return widget
                  if (list.isEmpty) {
                    return Center(
                      child: Text(
                        Strings.taskViewLabel,
                        style: TextStyle(
                          color: MyColors.darkColor,
                        ),
                      ),
                    );
                  }
                  //
                  else {
                    return ListView.builder(
                      itemCount: list.length,
                      itemBuilder: (context, index) {
                        return TaskTile(list[index]);
                      },
                    );
                  }
                }
              },
            );
          },
        ),
      ),
    );
  }
}
