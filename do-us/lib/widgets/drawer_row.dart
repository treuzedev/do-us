import 'package:dome/classes/colors.dart';
import 'package:dome/classes/sizes.dart';
import 'package:flutter/material.dart';

//
// drawer menu row
class DrawerRow extends StatelessWidget {
  //
  // constructor
  DrawerRow({
    @required this.blockSize,
    @required this.text,
    @required this.icon,
    @required this.onTap,
  });

  final double blockSize;
  final String text;
  final IconData icon;
  final Function onTap;

  //
  // build method
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Sizes.getGeneralPadding(blockSize),
      child: InkWell(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: Icon(
              icon,
              color: MyColors.darkColor,
            )),
            Expanded(
              child: Text(
                text,
                style: TextStyle(
                  color: MyColors.darkColor,
                ),
              ),
            ),
          ],
        ),
        onTap: onTap,
      ),
    );
  }
}
