import 'package:dome/classes/colors.dart';
import 'package:dome/classes/my_navigator.dart';
import 'package:dome/classes/shared_view_model.dart';
import 'package:dome/classes/strings.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'drawer_row.dart';

//
// my drawer menu
class MyDrawerMenu extends StatelessWidget {
  //
  // constructor
  MyDrawerMenu({
    @required this.blockSize,
  });

  final double blockSize;

  //
  // build method
  @override
  Widget build(BuildContext context) {
    return Consumer<SharedViewModel>(
      builder: (context, sharedViewModel, child) {
        return Drawer(
          child: Container(
            color: MyColors.lightColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                DrawerRow(
                  blockSize: blockSize,
                  text: Strings.homeLabel,
                  icon: Icons.home,
                  onTap: () {
                    //
                    // pop and navigate to contacts
                    Navigator.pop(context);
                    Navigator.popAndPushNamed(
                      context,
                      MyNavigator.homeRoute,
                    );
                  },
                ),
                DrawerRow(
                  blockSize: blockSize,
                  text: Strings.aboutLabel,
                  icon: Icons.help,
                  onTap: () {
                    //
                    // pop and navigate to about
                    Navigator.pop(context);
                    Navigator.popAndPushNamed(
                      context,
                      MyNavigator.aboutRoute,
                    );
                  },
                ),
                DrawerRow(
                  blockSize: blockSize,
                  text: Strings.contactsLabel,
                  icon: Icons.account_circle,
                  onTap: () {
                    //
                    // pop and navigate to contacts
                    Navigator.pop(context);
                    Navigator.popAndPushNamed(
                      context,
                      MyNavigator.contactsRoute,
                    );
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
