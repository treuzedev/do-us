import 'package:dome/classes/colors.dart';
import 'package:dome/classes/shared_view_model.dart';
import 'package:dome/classes/sizes.dart';
import 'package:dome/classes/strings.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../classes/helpers.dart';
import '../classes/my_navigator.dart';
import '../database/database_helpers.dart';

//
// do us counter
class DuUsCounter extends StatelessWidget {
  //
  // constructor
  DuUsCounter(this.blockSize);

  final double blockSize;

  //
  // return widget
  @override
  Widget build(BuildContext context) {
    return Consumer<SharedViewModel>(
      builder: (context, sharedViewModel, child) {
        return Center(
          child: Padding(
            padding: Sizes.getCounterContainerPadding(blockSize),
            child: Material(
              color: MyColors.darkColor,
              borderRadius: Sizes.getGeneralBorderRadius(blockSize),
              child: InkWell(
                onLongPress: () {
                  //
                  // show dialog
                  Helpers.showMyDialog(
                    context: context,
                    blockSize: blockSize,
                    title: Strings.resetDialog,
                    yesOnPressed: () async {
                      //
                      // delete all tasks
                      await DatabaseHelpers.updateCounter(
                        sharedViewModel: sharedViewModel,
                        oldValue: '-1',
                      );

                      //
                      // go back
                      Navigator.pop(context);
                      Navigator.popAndPushNamed(context, MyNavigator.homeRoute);
                    },
                    noOnPressed: () {
                      //
                      // pop dialog
                      Navigator.pop(context);
                    },
                  );
                },
                child: Padding(
                  padding: Sizes.getGeneralPadding(blockSize),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        Strings.homeText,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        ' ' +
                                sharedViewModel
                                    .getCounterValue(sharedViewModel) ??
                            Strings.loadingLabel,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
