import 'package:dome/classes/sizes.dart';
import 'package:flutter/material.dart';

//
// a round material button
class RoundMaterialButton extends StatelessWidget {
  //
  // constructor
  RoundMaterialButton(
      {@required this.onPressed,
      @required this.text,
      @required this.color,
      @required this.blockSize});

  final Function onPressed;
  final Color color;
  final String text;
  final double blockSize;

  //
  // build method
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Sizes.getButtonPadding(blockSize),
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: Sizes.getGeneralBorderRadius(blockSize),
        ),
        child: MaterialButton(
          child: Text(
            text,
            style: TextStyle(color: Colors.white),
          ),
          onPressed: onPressed,
        ),
      ),
    );
  }
}
