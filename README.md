# Do Us

## Store your TODOS'S!

A Flutter app to store your todos.

<br>

### Features:

* Add a task;
* Mark a task completed;
* Edit a task;
* Delete a task.

<br>

#### Examples Below:

<img src="screenshots/screenshots1.jpg" height="300" width="150"> <img src="screenshots/screenshots2.jpg" height="300" width="150"> <img src="screenshots/screenshots3.jpg" height="300" width="150">
<br>
<img src="screenshots/screenshots5.jpg" height="300" width="150"> <img src="screenshots/screenshots4.jpg" height="300" width="150">
<br>

##### Inspired by Udemy's The Complete 2020 Flutter Development Bootcamp with Dart Section 16.
